<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;

class IngresoController extends Controller
{

    //1 usuario General
    // 2 admin del sitio
    // 3 admin depto 
    // 4 recursos humanos
    // 5 usuario externo

    public function ingreso()
    {
        $usuarioID = auth()->id();
        $usuario = User::find($usuarioID);
        foreach ($usuario->roles as $rol)
        {
            if($rol->id == 2)
            {
                Session::put('admin', 2);
            }elseif
            ($rol->id == 3)
            {
                Session::put('admin-depto', 3);
            }elseif
            ($rol->id == 4)
            {
                Session::put('recursos-humanos', 4);
            }
             
        }
        //return view ('principal');
        return redirect ('misLicencias');
        
    }
}
