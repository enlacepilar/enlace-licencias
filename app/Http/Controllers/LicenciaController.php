<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Licencia;
Use App\Http\Controllers\UsuarioController;

class LicenciaController extends Controller
{
    public $paginar = 15;

    public function nuevaLicencia ()
    {
#region Usuario
        $usuarioID = auth()->id();
        $usuario = User::find($usuarioID);
        
        return view ('usuario.licencias.nueva-licencia', ['usuario'=>$usuario] );
        
    }

    public function recibeLicencia (Request $request)
    {
        //Ver estados para cotejar con los numeros
        $licenciaNueva = request()->except('_token');
        $licenciaNueva['created_at'] = date('Y-m-d H:i:s');
        $licenciaNueva['estado_id'] = 1;
        Licencia::insert($licenciaNueva);
        $datos = "Licencia cargada";

        $usuarioID = auth()->id();
        $usuario = User::find($usuarioID);
        
        return view ('usuario.licencias.nueva-licencia', ['usuario'=>$usuario, 'datos'=>$datos] );
    }

    public function misLicencias ()
    {
        $usuarioID = auth()->id();
        $tramites = Licencia::where('user_id', $usuarioID)->paginate($this->paginar);
        
        return view ('usuario.licencias.mis-licencias', ['tramites'=>$tramites]);
    }

    public function modificaTramiteLicencia (Request $request)
    {
        $idTramite = $request->get('idTramite');
        $tramite = Licencia::find ($idTramite);
        
        return view ('usuario.licencias.modificar-tramite-licencia', ['tramite'=>$tramite]);
    }

    public function recibeModificarTramiteLicencia (Request $request)
    {
        $request->validate([    
            'archivo_adjunto'=>'|file|max:5000|mimes:pdf'
        ]);

        $licenciaModifica = request()->except('_token');
              
        $actualizaTramite = Licencia::find($request->get('idTramite'));

        if (isset($licenciaModifica['archivo_adjunto']))
        {
            $nombreArchivo = rand (0, 999) . $request->file('archivo_adjunto')->getClientOriginalName();
            $urlArchivo = "/storage/documentos_pdf/". $nombreArchivo;
            $request->file('archivo_adjunto')->storeAs('public/documentos_pdf', $nombreArchivo);
            $licenciaModifica['archivo_adjunto']  = $urlArchivo;
            $actualizaTramite->archivo_adjunto =  $licenciaModifica['archivo_adjunto'];
        }

        if (isset($licenciaModifica['licencia_desde']) && isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_desde = $licenciaModifica['licencia_desde'];
            $actualizaTramite->licencia_hasta = $licenciaModifica['licencia_hasta'];
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }
        elseif (!isset($licenciaModifica['licencia_desde']) && isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_hasta = $licenciaModifica['licencia_hasta'];
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }
        elseif (isset($licenciaModifica['licencia_desde']) && !isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_desde = $licenciaModifica['licencia_desde'];
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }else
        {
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }
        
        $datos = "Información actualizada";
        $idTramite = $request->get('idTramite');

        $tramite = Licencia::find ($idTramite);
        return view ('usuario.licencias.modificar-tramite-licencia', ['tramite'=>$tramite, 'datos'=>$datos]);
    }
#endregion

#region Administrador
    public function adminLicencias ()
    {
        $idAdmin = auth()->id();
        $usuAdmin = User::find($idAdmin); 
        
        foreach ($usuAdmin->roles as $rol)
        {
            if ($rol->id == 2) // si es administrador del sitio
            {
                $tramites = Licencia::Paginate($this->paginar);    
                return view ('admin.licencias.listado-licencias', ['tramites'=>$tramites]);        
            }else
            {
                $tramites = Licencia::where('departamento_id', $usuAdmin->departamento_id)->paginate($this->paginar);
                return view ('admin.licencias.listado-licencias', ['tramites'=>$tramites]);
            }
        }
        
    }

    public function adminLicenciasPorUsuario (Request $request)
    {
        $idAdmin = auth()->id();
        $usuAmin = User::find($idAdmin);
        
        $id = $request->get('id');
        $tramites = Licencia::where('user_id', $id)->where('departamento_id', $usuAmin->departamento_id)->paginate($this->paginar);
        $usuario = User::where('id', $id)->get();
        
        return view ('admin.licencias.licencias-por-usuario', ['tramites'=>$tramites, 'usuario'=>$usuario]);
    }

    public function actualizaLicenciaAdmin(Request $request)
    {
        $idTramite = $request->get('idTramite');
        $tramite = Licencia::find ($idTramite);
        
        return view ('admin.licencias.modificar-tramite-licencia-admin', ['tramite'=>$tramite]);
    }

    public function recibeModificarTramiteLicenciaAdmin (Request $request)
    {
        $request->validate([    
            'archivo_adjunto'=>'|file|max:5000|mimes:pdf'
        ]);

        $licenciaModifica = request()->except('_token');
        
        $actualizaTramite = Licencia::find($request->get('idTramite'));

        if (isset($licenciaModifica['archivo_adjunto']))
        {
            $nombreArchivo = rand (0, 999) . $request->file('archivo_adjunto')->getClientOriginalName();
            $urlArchivo = "/storage/documentos_pdf/". $nombreArchivo;
            $request->file('archivo_adjunto')->storeAs('public/documentos_pdf', $nombreArchivo);
            $licenciaModifica['archivo_adjunto']  = $urlArchivo;
            $actualizaTramite->archivo_adjunto =  $licenciaModifica['archivo_adjunto'];
        }

        if (isset($licenciaModifica['licencia_desde']) && isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_desde = $licenciaModifica['licencia_desde'];
            $actualizaTramite->licencia_hasta = $licenciaModifica['licencia_hasta'];
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }
        elseif (!isset($licenciaModifica['licencia_desde']) && isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_hasta = $licenciaModifica['licencia_hasta'];
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }
        elseif (isset($licenciaModifica['licencia_desde']) && !isset($licenciaModifica['licencia_hasta'])  )
        {
            $actualizaTramite->licencia_desde = $licenciaModifica['licencia_desde'];
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }else
        {
            $actualizaTramite->motivo = $licenciaModifica['motivo'];
            $actualizaTramite->save();
        }
        
        $datos = "Información actualizada";
        $idTramite = $request->get('idTramite');
        $tramite = Licencia::find ($idTramite);
        
        return view ('admin.licencias.modificar-tramite-licencia-admin',['tramite'=>$tramite, 'datos'=>$datos]);
        
    }

    public function aprobarLicencia(Request $request)
    {
        $idAdmin = auth()->id();
        $licencia = Licencia::find($request->json('idLicencia'));
        


        if ($idAdmin === $licencia->user_id)
        {
            $mensaje = "No puede aprobar su propia licencia";    
        }else
        {
            $licencia->estado_id = 2;
            $licencia->save();

            $mensaje = "Licencia aprobada.";
        }
        
        
        return response()->json( $mensaje );
    }

#endregion

#region Administracion - Externos

    public function aprobarLicenciaExterno ($id)
    {
        //comando prueba
        //$comando = "./markpdf 'osdem.pdf' 'colab.png' 'output4.pdf' --opacity=0.3 --scale-width";

        //guardar el nombre del archivo en la licencia tambien
        $licencia = Licencia::find($id);
        if ($licencia->archivo_adjunto !=null)
        {
            $rutaArchivoPDF = ".".$licencia->archivo_adjunto; 
            $aprobado = "lincenciaId-$id-aprobada.pdf";
            $rutaLicenciaAprobada = "../aprobados/$aprobado";
            //dd($rutaArchivoPDF);

            try
            {
                $ejecuta = exec("./aprobados/markpdf '$rutaArchivoPDF' 'aprobados/ultimos-libros.png' 'aprobados/$aprobado' --opacity=0.3 --scale-width");
        
                
                
                return "Licencia Aprobada!!! Mensaje ---> $ejecuta <br><br> <a href='$rutaLicenciaAprobada'>Ver archivo</a>";

            }catch(exception $error)
            {
                echo json_encode(array('resultado'=>'<h1>Ocurrió un error. Será reedirigido<h1>'));
                //echo ('ERROr catch envio mail');
    
            }

            
            
        }
        else{
            return "La licencia no posee adjunto todavia.";
        }
        
    }
#endregion
}
