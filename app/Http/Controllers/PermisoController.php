<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\Permiso;
Use App\Http\Controllers\UsuarioController;


class PermisoController extends Controller
{
    public $paginar = 15;
#region Usuario 
    public function nuevoPermiso ()
    {
        $usuarioID = auth()->id();        
        $usuario = User::find($usuarioID);

        return view ('usuario.permisos.nuevo-permiso', ['usuario'=>$usuario] );
    }

    public function recibePermiso (Request $request)
    {
        //Ver estados para cotejar con los numeros
        $permisoNuevo = request()->except('_token');
        $permisoNuevo['created_at'] = date('Y-m-d H:i:s');
        $permisoNuevo['estado_id'] = 1;
        Permiso::insert($permisoNuevo);
        $datos = "Solicitud de Permiso cargado";

        $usuarioID = auth()->id();
        $usuario = User::find($usuarioID);
        
        return view ('usuario.permisos.nuevo-permiso', ['usuario'=>$usuario, 'datos'=>$datos] );
    }

    public function misPermisos ()
    {
        $usuarioID = auth()->id();
        $tramites = Permiso::where('user_id', $usuarioID)->paginate($this->paginar);
     
        return view ('usuario.permisos.mis-permisos', ['tramites'=>$tramites]);
    }

    public function modificaTramitePermiso (Request $request)
    {
        $idTramite = $request->get('idTramite');
        $tramite = Permiso::find ($idTramite);
        
        return view ('usuario.permisos.modificar-tramite-permiso', ['tramite'=>$tramite]);
    }

    public function recibeModificarTramitePermiso (Request $request)
    {
        $request->validate([    
            'archivo_adjunto'=>'|file|max:5000|mimes:pdf, jpeg,png,jpg'
        ]);

        $permisoModifica = request()->except('_token');       
        $actualizaPermiso = Permiso::find($request->get('idTramite'));

        if (isset($permisoModifica['archivo_adjunto']))
        {
            $nombreArchivo = rand (0, 999) . $request->file('archivo_adjunto')->getClientOriginalName();
            $urlArchivo = "/storage/documentos_pdf/". $nombreArchivo;
            $request->file('archivo_adjunto')->storeAs('public/documentos_pdf', $nombreArchivo);
            $permisoModifica['archivo_adjunto']  = $urlArchivo;
            $actualizaPermiso->archivo_adjunto =  $permisoModifica['archivo_adjunto'];
        }

        if (isset($permisoModifica['fecha_permiso']) && isset($permisoModifica['motivo']))
        {
            $actualizaPermiso->permiso_desde = $permisoModifica['fecha_permiso'];
            $actualizaPermiso->motivo = $permisoModifica['motivo'];
            $actualizaPermiso->save();
        }
        elseif (!isset($permisoModifica['fecha_permiso']) && isset($permisoModifica['motivo'])  )
        {
            $actualizaPermiso->motivo = $permisoModifica['motivo'];
            $actualizaPermiso->save();
        }
        elseif (isset($permisoModifica['fecha_permiso']) && !isset($permisoModifica['motivo'])  )
        {
            $actualizaPermiso->permiso_desde = $permisoModifica['fecha_permiso'];
            $actualizaPermiso->save();
        }else
        {
            $actualizaPermiso->save();  
        }
        
        $datos = "Información actualizada";
        $idTramite = $request->get('idTramite');

        $tramite = Permiso::find ($idTramite);
        return view ('usuario.permisos.modificar-tramite-permiso', ['tramite'=>$tramite, 'datos'=>$datos]);

    }
#endregion

#region Administrador
    public function adminPermisos()
    {
        $idAdmin = auth()->id();
        $usuAdmin = User::find($idAdmin);      
        
        foreach ($usuAdmin->roles as $rol)
        {
            if ($rol->id == 2) // si es administrador del sitio
            {
                $tramites = Permiso::Paginate($this->paginar);    
                return view ('admin.permisos.listado-permisos', ['tramites'=>$tramites]);        
            }else
            {
                $tramites = Permiso::where('departamento_id', $usuAdmin->departamento_id)->paginate($this->paginar);
                return view ('admin.permisos.listado-permisos', ['tramites'=>$tramites]);
            }
        }

    }

    public function adminPermisosPorUsuario (Request $request)
    {
        $id = $request->get('id');
        $tramites = Permiso::where('user_id', $id)->paginate($this->paginar);
        
        return view ('admin.permisos.permisos-por-usuario', ['tramites'=>$tramites]);
    }

    public function modificaTramitePermisoAdmin (Request $request)
    {
        $idTramite = $request->get('idTramite');
        $tramite = Permiso::find ($idTramite);
        
        return view ('admin.permisos.modificar-tramite-permiso', ['tramite'=>$tramite]);
    }

    public function recibeModificarTramitePermisoAdmin (Request $request)
    {
        $request->validate([    
            // 'archivo_adjunto'=>'|file|max:5000|mimes:pdf',
            'archivo_adjunto'=>'|image|max:1500|mimes:jpeg,png,jpg'
        ]);

        $permisoModifica = request()->except('_token');
               
        $actualizaPermiso = Permiso::find($request->get('idTramite'));

        if (isset($permisoModifica['archivo_adjunto']))
        {
            $nombreArchivo = rand (0, 999) . $request->file('archivo_adjunto')->getClientOriginalName();
            $urlArchivo = "/storage/documentos_pdf/". $nombreArchivo;
            $request->file('archivo_adjunto')->storeAs('public/documentos_pdf', $nombreArchivo);
            $permisoModifica['archivo_adjunto']  = $urlArchivo;
            $actualizaPermiso->archivo_adjunto =  $permisoModifica['archivo_adjunto'];
        }

        if (isset($permisoModifica['fecha_permiso']) && isset($permisoModifica['motivo']))
        {
            $actualizaPermiso->permiso_desde = $permisoModifica['fecha_permiso'];
            $actualizaPermiso->motivo = $permisoModifica['motivo'];
            $actualizaPermiso->save();
        }
        elseif (!isset($permisoModifica['fecha_permiso']) && isset($permisoModifica['motivo'])  )
        {
            $actualizaPermiso->motivo = $permisoModifica['motivo'];
            $actualizaPermiso->save();
        }
        elseif (isset($permisoModifica['fecha_permiso']) && !isset($permisoModifica['motivo'])  )
        {
            $actualizaPermiso->permiso_desde = $permisoModifica['fecha_permiso'];
            $actualizaPermiso->save();
        }else
        {
           
            $actualizaPermiso->save();
           
        }
        
        $datos = "Información actualizada";
        $idTramite = $request->get('idTramite');

        $tramite = Permiso::find ($idTramite);
        return view ('admin.permisos.modificar-tramite-permiso', ['tramite'=>$tramite, 'datos'=>$datos]);
    }

    public function aprobarPermiso(Request $request)
    {
        $idAdmin = auth()->id();
        $permiso = Permiso::find($request->json('idPermiso'));
        
        if ($idAdmin === $permiso->user_id)
        {
            $mensaje = "No puede aprobar su propio permiso";    
        }else
        {
            $permiso->estado_id = 2;
            $permiso->save();

            $mensaje = "Permiso aprobado.";
        }
        return response()->json($mensaje);
    }

#endregion

}
