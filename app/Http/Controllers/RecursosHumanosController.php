<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Licencia;
use App\Models\Permiso;

class RecursosHumanosController extends Controller
{
    public function licencias()
    {
        $licencias = Licencia::Where('estado_id', 2)->Paginate(20);
        return view ('recursoshumanos.licencias', ['tramites'=>$licencias]);
    }

    public function permisos()
    {
        $permisos = Permiso::where('estado_id', 2)->Paginate(20);
        return view ('recursoshumanos.permisos', ['tramites'=>$permisos]);
    }
}
