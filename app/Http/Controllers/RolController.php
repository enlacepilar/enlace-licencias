<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rol;
use App\Models\User;

// ROLES
// ********************
// 1 - Usuario General
// 2 - Administrador del Sitio
// 3 - Administrador Departamento
// 4 - Recursos Humanos
// 5 - Usuario Externo
// 6 - Gerencia de Departamento

class RolController extends Controller
{
    public function crearRoles()
    {
        $roles = Rol::all();

        if (count($roles)<1)
        {
            $rol = new Rol;
            $rol->nombre = "Usuario General";
            $rol->save();

            $rol = new Rol;
            $rol->nombre = "Administrador del sitio";
            $rol->save();

            $rol = new Rol;
            $rol->nombre = "Administrador Departamento";
            $rol->save();

            $rol = new Rol;
            $rol->nombre = "Recursos Humanos";
            $rol->save();

            $rol = new Rol;
            $rol->nombre = "Usuario Externo";
            $rol->save();

            echo "Roles creados exitosamente";
        }
        else{
            echo "Los roles ya fueron creados anteriormente.";
        }
    }

    public function agregarRoles()
    {
        $roles = Rol::all();
        $rolGerencia = 'crealo';
        
        foreach ($roles as $rol)
        {
            if ($rol->nombre == "Gerencia de Departamento")
            {
                $rolGerencia = "El rol ya habia sido creado";
            }
        }

        if ($rolGerencia == 'crealo')
        {
            $rol = new Rol;
            $rol->nombre = "Gerencia de Departamento";
            $rol->save();

            return "Rol *Gerencia de Departamento* creado exitosamente";
        }else
        {
            return $rolGerencia;
        }  
    }

    function roles_de_un_usuario(Request $request) 
    {
        $usuario = User::find ($request->get('ide'));
        $roles = Rol::all();

        $rolesUsu = [];
        $rolesTotales = [];

        foreach ($usuario->roles as $rol)
        {
            $rolesUsu[$rol->id]=$rol->nombre;
        }

        foreach ($roles as $rol2)
        {
            $rolesTotales[$rol2->id]=$rol2->nombre;
        }
        $rolesRestantes = array_diff($rolesTotales, $rolesUsu);
        
        return view('admin.roles', ['usuario'=>$usuario, 'roles'=>$rolesRestantes]);
    }

    function modificaRoles(Request $request)
    {
        $original = $request->get('original');
        $sacar = $request->get('sacar');
        $poner = $request->get('poner');
        $usuId = $request->get('usuId');

        $datosQuitar = '';
        $datosAgregar = '';
        
        $usuario = User::find($usuId);

        if ($sacar == null)
        {
            $sacar = []; //si no tiene roles para quitar transformo el null en un array para poder comparar
        }

        $diferencia = array_diff($original, $sacar);
        
        if ($diferencia == null)
        {
            $datosQuitar = "No se han quitado roles.";
            
        }
        else{

            foreach ($diferencia as $rol)
            {
                $usuario->roles()->detach($rol);
            }
            $datosQuitar = "Se han quitado roles.";
        }
        //dd($poner);
        if ($poner != null)
        {
            
            foreach ($poner as $rol)
            {
                $usuario->roles()->attach($rol);
            }
            $datosAgregar = "Roles agregados.";
        }

        $datos = $datosQuitar . " " . $datosAgregar;
    
        $idAdmin = auth()->id();
        $usuAmin = User::find($idAdmin);
        $usuarios = User::paginate(15); 


        return view ('admin.lista-de-usuarios', ['usuarios'=> $usuarios, 'datos'=>$datos]);
        
    }
}
