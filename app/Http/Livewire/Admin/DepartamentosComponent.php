<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Departamento;
use App\Models\User;

class DepartamentosComponent extends Component
{
    use WithPagination;
    public $paginationTheme = "bootstrap";

    public $departamento;

    public function render()
    {
        $departamentos = Departamento::all();
        
        $idUsuario = auth()->id();
        $usuario = User::find($idUsuario);

        return view('livewire.admin.departamentos-component', ['departamentos'=>$departamentos, 'usuario'=>$usuario]);
    }

    public function agregaDepartamento()
    {
        if ($this->departamento == "")
        {
            session()->flash('mensajeError', 'Complete el campo para agregar un nuevo sector.');    
        }else
        {
            $nuevoDepto = new Departamento;
            $nuevoDepto->nombre = $this->departamento;
            $nuevoDepto->save();

            session()->flash('mensajeOk', 'Sector creado.');
        }
        
    }

    public function borrarDepartamento ($idDepartamento)
    {
        Departamento::destroy($idDepartamento);
        session()->flash('mensajeBorrado', 'Sector borrado.');
    }
}
