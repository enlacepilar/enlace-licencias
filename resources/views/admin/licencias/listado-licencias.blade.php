@extends('layouts.principal_raiz')
@section('contenido_app')
<div class="container mt-4">

    @if (isset($datos))
        @include('resultados.cargaBien')
    @endif

   
        <h1>Lista de licencias solicitadas por fecha y usuario</h1>
    
    @if (count($tramites)>0)

    <table class="table table-info table-bordered table-hover">
        <thead>
            <tr>
                <th>Fecha Alta:</th>
                <th>Usuario</th>
                <th>Tipo de trámite</th>
                <th>Sector</th>
                <th>Inicio licencia</th>
                <th>Fin</th>
                <th>Archivo</th>
                <th>Modificar</th>
                <th>Estado</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($tramites as $tramite )
            <tr>
                <td>{{date("d/m/Y", strtotime($tramite->created_at))}}</td>
                
                @if (isset(($tramite->user->name)))
                    <td>{{$tramite->user->name}}</td> 
                @else
                      <td><i>Sin nombre o eliminado</i></td>
                @endif

                <td>{{$tramite->tipo_tramite}}</td>
                @if (isset($tramite->departamento->nombre))
                    <td>{{$tramite->departamento->nombre}}</td>  
                @else
                    <td>Sin sector</td>  
                @endif
                
                @if (isset($tramite->licencia_desde))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_desde))}}</td>
                @else
                    <td>Sin fecha de inicio</td>
                @endif
            
                @if (isset($tramite->licencia_hasta))
                    <td>{{date("d/m/Y", strtotime($tramite->licencia_hasta))}}</td>
                @else
                    <td>Sin fecha de fin</td>
                @endif
            
                @if (isset($tramite->archivo_adjunto))
                    <td><a href="{{$tramite->archivo_adjunto}}" target="_blank">Abrir archivo</a></td>
                @else
                    <td>Sin archivo adjunto</td>
                @endif 
                
                {{-- @if (isset($tramite->archivo_adjunto) || (!isset(($tramite->user->name)))) --}}
                @if (!isset(($tramite->user->name)))
                    <td><button class="btn btn-warning" disabled>No modificable</button></td>
                @else
                    <td>
                        <form action="/actualizaLicenciaAdmin" method="get">
                            <input type="hidden" value="{{$tramite->id}}" name="idTramite">
                            <button type="submit" class="btn btn-warning"><i class="fas fa-edit"></i></button>
                        </form>
                    </td>
                @endif 

                @if ($tramite->estado_id == 1)
                    <td><button class="btn btn-success" onclick="aprobarLicencia({{$tramite->id}})">Aprobar</button></td>
                @elseif ($tramite->estado_id == 2)
                    <td><b class="text-primary"><i>Aprobada</i></b></td>
                @else
                    <td><b><i>{{$tramite->estado->nombre}}</i></b></td>
                @endif
                
            @endforeach   
                </tr>
        </tbody>
    </table>

    {{$tramites->links()}}
    @else
        <h2 class="text-center text-primary">No posee licencias</h2>    
    @endif
</div>
@endsection

<!-- Llamo al archivo para la consulta asicrona -->
<script src="/js/aprobarLicencia.js"></script>