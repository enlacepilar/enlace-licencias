@extends('layouts.principal_raiz')
@section('contenido_app')
<div class="container mt-4 alert-info p-3">
     
    <h1>Roles por usuario</h1>
    <hr>
        
    <div class="animate__animated animate__bounceInRight mt-1 p-5 alert-info">

        <div class="container table-responsive">
          
            <table class="table table-bordered table-primary">
            <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Legajo</th>
                  <th>Roles</th>
                </tr>
              </thead>
              
              <tbody>
                <tr>
                    <td>{{$usuario->name}}</td>
                    <td>{{$usuario->socio}}</td>
                
                    <td class="text-left">
                        <form action="modificaRoles" method="POST">
                        @csrf
                        <input type="hidden" name="usuId" value="{{$usuario->id}}">
                        
                        @if (count($usuario->roles) >0)
                            
                            @foreach($usuario->roles as $rol1)
                                <input type="hidden" name="original[]" value="{{$rol1->id}}">
                                <input type="checkbox" name="sacar[]" checked value="{{$rol1->id}}"> {{$rol1->nombre}} <br>
                            @endforeach
                            
                            @foreach ($roles as $id=>$nombre )    
                                <input type="checkbox" name="poner[]" value="{{$id}}"> {{$nombre}} <br>
                            @endforeach        
                            
                        @else
                            @foreach ($roles as $id=>$nombre )
                                <input type="hidden" name="original[]">
                                <input type="hidden" name="sacar[]">
                                <input type="checkbox" name="poner[]" value="{{$id}}"> {{$nombre}} <br>
                            @endforeach 
                        @endif
                    
                    </td>
                </tr>
                </tbody>
            </table>
            
            <br>
                        <input type="submit" class="btn btn-primary" value="Modificar">
                    </form>
        </div>
      
      </div>

</div>
@endsection