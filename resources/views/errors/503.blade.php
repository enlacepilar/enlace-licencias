@extends('errors::minimal')

@section('title', __('Servicio no disponible'))
@section('code', '503')
@section('message', __('El servicio no está disponible en estos momentos.'))
