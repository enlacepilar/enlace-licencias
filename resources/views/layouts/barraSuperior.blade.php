<nav class="navbar navbar-expand-lg navbar-light bg-info">
  <a class="navbar-brand  animate__animated animate__bounce" href="/"><img src="/imagenes/logo1.png" width="300px" class="img-fluid" alt=""></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav" id="items-navegacion">
            <li class="nav-item">
                <a class="nav-link" href="/datosUsuario" id="misDatos">Mis Datos</a>
            </li>
    
            <li class="nav-item">
                <a class="nav-link" href="/misLicencias" id="misLicencias">Mis Licencias</a>
            </li>
    
            <li class="nav-item">
                <a class="nav-link" href="/misPermisos" id="misPermisos">Mis Permisos</a>
            </li>
            

            @if (Session::get('admin'))                    
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Menú Admin.
                    </a>
                    <div class="dropdown-menu animate__animated animate__fadeInUp" aria-labelledby="navbarDropdownMenuLink">
                        <a href="adminDatosUsuario" class="dropdown-item alert-warning">Ver/crear Usuarios</a>
                        <a href="adminPermisos" class="dropdown-item alert-warning">Solicitudes Permisos</a>
                        <a href="adminLicencias" class="dropdown-item alert-warning">Solicitudes Licencias</a>
                        
                    </div>
                </li>
            @endif

            @if (Session::get('admin-depto'))
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Solicitudes
                    </a>
                    <div class="dropdown-menu animate__animated animate__fadeInUp" aria-labelledby="navbarDropdownMenuLink">
                        <a href="adminPermisos" class="dropdown-item alert-warning">Solicitudes Permisos</a>
                        <a href="adminLicencias" class="dropdown-item alert-warning">Solicitudes Licencias</a> 
                    </div>
                </li>
            @endif


            @if (Session::get('recursos-humanos'))
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Recursos Humanos
                    </a>
                    <div class="dropdown-menu animate__animated animate__fadeInUp" aria-labelledby="navbarDropdownMenuLink">
                        <a href="rrhhPermisos" class="dropdown-item alert-warning">Solicitudes Permisos</a>
                        <a href="rrhhLicencias" class="dropdown-item alert-warning">Solicitudes Licencias</a> 
                    </div>
                </li>
            @endif
            

        
        </ul>
    </div>

      <!-- Right Side Of Navbar -->
        <ul style="float:right;">            
        <!-- Authentication Links -->
            @guest
            @if (Route::has('login'))
                <li class="nav-item" style="float:left;">
                    <a class="nav-link" href="/login"><span class="text-light"><i class="fas fa-user-lock"></i> {{ __('login.login2') }}</span></a>
                </li>
                <li class="nav-item" style="float:right;">
                    <a class="nav-link" href="/register"><span class="text-light"><i class="fas fa-user-astronaut"></i> Registrarse</span></a>
                </li>
            @endif

            @else
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Usuario: {{ Auth::user()->name }}
                    </a>
                    
                    <div class="dropdown-menu animate__animated animate__fadeInUp" aria-labelledby="navbarDropdownMenuLink">
                        {{-- <a class="dropdown-item alert-warning" href="/adminDatosUsuario">Ediar datos</a> --}}
                        
                        @if (Session::get('admin'))  
                            <a class="dropdown-item alert-warning" href="/sectores">Sectores</a>
                        @endif
                        
                        <a class="dropdown-item alert-warning" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                           Salir
                        </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                    </div>
                </li>


            @endguest
        </ul>

       


</nav>









  