<div>
@if (isset($usuario->departamento_id))
    <h2>No tiene los roles necesarios para modificar los sectores</h2>
@else
    @if (session()->has('mensajeOk'))
        <div class="animate__animated animate__flash alert alert-success text-center mb-3">
            <h3><b>{{ session('mensajeOk') }}</b></h3>
        </div>
    @elseif (session()->has('mensajeError'))
        <div class="animate__animated animate__flash alert alert-danger text-center mb-3">
            <h3><b>{{ session('mensajeError') }}</b></h3>
        </div>
    @elseif (session()->has('mensajeBorrado'))
        <div class="animate__animated animate__flash alert alert-warning text-center mb-3">
            <h3><b>{{ session('mensajeBorrado') }}</b></h3>
        </div>
    @endif

    <div class="row">
        <div class="col-5" style="border-right: 1px solid black;">
            <label for="sector"><b><i>Agregar sector</i></b></label>
            <input type="text" id="sector" class="form-control" wire:model="departamento">
            <br>
            <button class="btn btn-primary" wire:click="agregaDepartamento">Agregar sector</button>        
        </div>
        
        <div class="col-6 text-center">
            @if (count($departamentos)>0)
            <table class="table table-bordered table-secondary">
                <thead class="alert-primary">
                    <tr>
                        <th>Nombre del sector</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($departamentos as $nombre)
                    <tr>
                        <td>{{$nombre->nombre}}</td>
                        <td><button class="btn btn-danger" wire:click="borrarDepartamento({{$nombre->id}})"><i class="far fa-trash-alt"></i></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <h2>No se han creado sectores</h2>
            @endif
        </div>

    </div>
@endif
</div>
